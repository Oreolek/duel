global "char" ({})
init = function ()
  std.dlg.noinv = true
  std.phrase_show = false
  game.player = std.menu_player {
    nam = 'player'
  }
  game.talk = 'Вы не знаете, что спросить.'
  game.take = function()
    pn('КАК?!')
    return false
  end
  char.quests = {}
  char.wounds = 0
  char.actions = 50
  char.reputation = 50
  char.can_leave = false
  char.quests.chos = 0
  place(menu {
    nam = "Персонаж",
    menu = function()
      walk('character')
    end
  }, me())
  place(menu {
    nam = "Карта",
    menu = function()
      walk('map')
    end
  }, me())
  place(menu {
    nam = "Квесты",
    menu = function()
      walk('quests')
    end
  }, me())
  place(menu {
    nam = 'телефон',
    disp = function()
      if char.call ~= nil then
        return fmt.b("Телефон")
      end
      return "Телефон"
    end,
    menu = function()
      walk('inventory')
    end
  }, me())
  place(menu {
    nam = "end-game",
    disp = function()
      if char.can_leave then
        return fmt.b("Уехать")
      end
      return false
    end,
    menu = function()
      walk('ending')
    end
  }, me())
  char.act = nil
  place( proxy_menu {
  	disp = function()
      if stead.table.maxn(here().obj) > 0 then
        return 'ОСМОТРЕТЬ';
      end
      return false
    end;
  	acts = { inv = 'act' };
		sources = { scene = true, inv = false }; -- осмотр инвентаря вынесен отдельно
  }, me())
  --[[
  place( proxy_menu {
		disp = 'ВЗЯТЬ';
		acts = { inv = 'take' };
		sources = { scene = true, inv = false };
  }, me())
  place( proxy_menu {
		disp = 'ВЫБРОСИТЬ';
		acts = { inv = 'drop' };
		sources = { scene = false, inv = true };
  }, me())
  place( proxy_menu {
		disp = 'ОТКРЫТЬ';
		acts = { inv = 'open' };
		sources = { scene = false, inv = true };
  }, me())
  place( proxy_menu {
		disp = 'ЗАКРЫТЬ';
		acts = { inv = 'close' };
		sources = { scene = false, inv = true };
  }, me())
  ]]
  place( proxy_menu {
  	disp = function()
      if stead.table.maxn( here().obj ) > 0 then
        return 'ГОВОРИТЬ С';
      end
      return false
    end;
		acts = { inv = 'talk' };
		sources = { scene = true, inv = false };
  }, me())
end
game.after_take = function(s, w)
  take(w)
end

game.after_drop = function(s, w)
  drop(w)
end
game.exam = 'Ничего необычного.'

engineer = function()
  if char.plural == true then
    return 'инженеры'
  end
  return 'инженер'
end
that = function()
  if char.plural == true then
    return 'те'
  end
  return 'тот'
end
pronoun_3 = function()
  if char.gender == 'she' then
    return 'она'
  end
  if char.gender == 'he' then
    return 'он'
  end
  if char.gender == 'it' then
    return 'оно'
  end
  return 'они'
end
pronoun_1 = function()
  if char.plural == true then
    return 'мы'
  end
  return 'я'
end
pronoun_2 = function()
  if char.plural == true then
    return 'вы'
  end
  return 'ты'
end
endings = function()
  if char.plural == true then
    return 'и'
  end
  if char.gender == 'she' then
    return 'а'
  end
  return ''
end
plural = function(a, b)
  if char.plural == true then
    return b
  end
  return a
end
rndstr = function(strings)
	return strings[rnd(stead.table.maxn(strings))];
end
rndalt = function(yes, no)
  if rnd(2) == 1 then
    return yes
  end
  return no
end
gendered = function(he, she, it, they)
  if char.gender == 'she' then
    return she
  end
  if char.gender == 'he' then
    return he
  end
  if char.gender == 'they' then
    return they
  end
  return it
end
wrong_gendered = function(he, she)
  if char.gender == 'she' then
    return he
  end
  return she
end
official = function()
  if char.gender == 'she' then
    return 'уважаемая'
  end
  if char.gender == 'he' then
    return 'уважаемый'
  end
  if char.gender == 'it' then
    return 'уважаемое'
  end
  return 'уважаемые'
end
opposed = function(value)
  local retval = '['
  for i=1,10,1 do
    retval = retval .. '='
    if math.floor(value/10) == i then
      retval = retval .. '*'
    end
  end
  retval = retval ..']'
  return retval
end
changeop = function(name, delta)
  rawset(char, name, fairadd(rawget(char, name), delta))
end
fairadd = function(value, add)
  return math.floor(value + (100-value)*(add/100))
end
listinv = function()
  local out = ''
  local comma = false
	for k, v in std.ipairs(inv()) do
  	if v.disp and not v:disabled() then
      if comma then
        out = out .. ', '
      else
        comma = true
      end
      out = out .. v.disp
	  end
  end
  if out == '' then
    out = 'ничего, кроме телефона.'
  else
    out = out .. '.'
  end
  return out
end
wounds = function()
  char.wounds = char.wounds + 1
end
