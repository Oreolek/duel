obj {
  nam = 'back',
  dsc = function() return fmt.c("{Назад}") end,
  act = function()
    return walkout()
  end
}

local markers = {
  chos = "626,666",
  abandoned = "264,213",
  exit_west = "373,540",
  pavels = "168,609",
  houseclosed = "348,692",
  exit_north = "448,157",
  intersection = "555,550",
  town = "543,462",
  factory = "703,310",
  cathedral = "703,310"
}
room {
  nam = 'map',
  disp = "Карта",
  noinv = true,
  enter = function (here, from)
    char.lastroom = from.nam
  end,
  pic = function()
    local map = 'images/satellite.jpg'
    if markers[char.lastroom] then
      map = map .. ';images/marker.png@' .. markers[char.lastroom]
    end
    return map
  end,
  decor = [[
    Вы сверяетесь с картой.
  ]],
  obj = {'back'}
}
dlg {
  nam = 'quests',
  disp = "Квесты",
  noinv = true,
  decor = [[
    Вы вспоминаете обо всём, что нужно сделать.
  ]],
  obj = {{
    {
      cond = function()
        return char.quests.chos == nil
      end,
      'Найти Ангелину Чо',
      'Вы приехали по гарантийному вызову. Вам надо найти Ангелину Чо, починить всё что у неё есть по гарантии и вернуться.'
    };
    {
      cond = function()
        return char.quests.car == 1
      end,
      'Вернуть автомобиль',
      'Неизвестный робот посчитал ваш автомобиль за бесхозную технику. Скорее всего, он где-то в этой деревне.'
    };
    {
      cond = function()
        return char.quests.car == 2
      end,
      'Вернуть автомобиль',
      'Вы нашли сообщество роботов. Нужно убедить их отдать вам автомобиль.'
    };
    {
      cond = function()
        return char.quests.car == 3
      end,
      'Вернуть автомобиль',
      'Просто дождитесь, когда его привезут.'
    };
    {
      cond = function()
        return char.quests.car == 4
      end,
      'Вернуть автомобиль',
      'Автомобиль здесь. Его никто не охраняет. Просто сядьте в машину.'
    };
    {
      cond = function()
        return char.quests.abandoned == 1
      end,
      'Бесхозные роботы',
      [[Мужчина в храме Последней Розы интересуется роботами на северной ферме.
      Их хозяин умер, и теперь ценные работники не подчиняются никому.
      Он хочет, чтобы вы настроили роботов на его голос.]]
    };
    {
      cond = function()
        return char.quests.chos == 1
      end,
      'Найти дом Павла',
      'Вы должны найти дом Павла и попросить у него программатор, чтобы починить Анжелине комбайн.'
    };
    {
      cond = function()
        return char.quests.chos == 2
      end,
      'Убедить Павла отдать программатор',
      'Старую технику надо искать у соседей.'
    };
    {
      cond = function()
        return char.quests.chos == 3
      end,
      'Починить комбайн… или нет?',
      'Починить Анжелине комбайн при помощи программатора.'
    };
    {
      cond = function()
        return char.quests.chos == -1
      end,
      '(провалено) Починить комбайн',
      'Вы отдали ваш единственный программатор в обмен на свою же автомашину.'
    };
    {
      'Назад',
      function() walkout() end,
    };
  }}
}

dlg {
  nam = 'inventory',
  disp = "Телефон",
  noinv = true,
  enter = function()
    local walkto = nil
    if char.call == 'unknown' then
      char.call = nil
      lifeoff('call-unknown')
      walk('caller-unknown')
    end
    return true
  end,
  decor = function()
    return 'Здесь будут все телефоны и заметки, которые вы запишете в телефон.'
  end,
  obj = {{
    {
      always = true,
      'Ангелина Чо',
      function()
        local retval = 'Вы набираете номер заказчика. Нет ответа.'
        if not visited('cho_dlg') then
          retval = retval .. ' Спит она там, что ли?'
        end
        return retval
      end,
    };
    {
      always = true,
      'Назад',
      function() walkout() end,
    };
  }}
}

room {
  nam = 'character',
  disp = 'Персонаж',
  noinv = true,
  decor = function()
    return [[Обращение: ]]..fmt.tab('20%')..pronoun_3()..
      [[^Действия: ]]..fmt.tab('20%')..
      'Оригинальность'..fmt.tab('40%')..opposed(char.actions)..' Традиционность'..
      fmt.tab('80%')..char.actions..'%'..
      [[^Репутация: ]]..fmt.tab('20%')..
      gendered('Нечестивец', 'Нечестивая', 'Нечестивое', 'Нечестивые')
      ..fmt.tab('40%')..opposed(char.reputation)..' '..
      gendered('Святой', 'Святая', 'Святое', 'Святые')..
      fmt.tab('80%')..char.reputation..'%'..
      [[^Раны: ]]..fmt.tab('20%')..char.wounds..[[ (при трёх ранах ]]..plural('персонаж умрёт', 'персонажи умрут')..')^'..
      [[^^При себе у вас: ]]..listinv()
  end,
  obj = {'back'}
}

room {
  nam = 'ending',
  disp = 'Уехать из Жестианы',
  noinv = true,
  decor = function()
    local out =  'Вы уверены, что хотите немедленно уехать из Жестианы?'
    if char.took_kathy then
      out = out .. [[(по пути вы не забудете заехать за девочкой)]]
    end
    out = out .. '^^' .. fmt.c('{yes|Да}')
    return out
  end,
  obj = {
    'back',
    obj {
      nam = 'yes',
      act = function() walk('theend') end,
    }
  }
}

room {
  nam = 'theend',
  disp = 'КОНЕЦ',
  noinv = true,
  decor = function()
    local out = 'Так закончилось ваше приключение в Жестиане.^^'
    if char.quests.chos < 4 then
      out = out .. [[Семья Чо потеряла урожай.
      После бурных споров она раскололась: прабабушка Анжелина с сыном остались в Жестиане,
      а молодое поколение перебралось в город. Роботов поделили пополам.
      Всей семье пришлось тяжело, но они выжили и даже немного рады переменам.^^]]
    else
      out = out .. [[Семья Чо собрала прекрасный урожай. ]]
      if char.took_kathy then
        out = out .. [[^^В городе Катя оказалась способной ученицей. Она с блеском закончила три курса
        теории и готовится к практической командировке. Куратором практики назначили вас.]]
      else
        out = out .. [[Но радость была недолгой, когда обнаружилось, что правнучка сбежала
        в город на подножке рейсового автобуса.]]
      end
      out = out .. '^^'
    end
    if char.robots_law == true then
      out = out .. [[Районное отделение полиции очень удивилось, когда обнаружило
      однорукого робота, который предлагал им зарегистрировать 45 «единиц находок»
      на большой потрёпанной тележке. Робота убедили в том, что находки действительно
      бесхозны, но на следующий день он вернулся с новой тележкой. После того,
      как среди механизмов обнаружились потерянная спутниковая рация полковника Косых
      и патроны для боевого механизированного костюма, полиция запротоколировала
      все «находки».^^]]
    else
      out = out .. [[Роботы Жестианы продолжают собирать бесхозные механизмы.
      Случайно обнаруженный ими искусственный интеллект позволил построить
      фабрику по производству новых братьев. Они всё ещё ждут хозяина.^^]]
    end
    out = out .. fmt.c(fmt.b('КОНЕЦ'))
    return out
  end
}
